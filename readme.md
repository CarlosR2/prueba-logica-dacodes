# Algoritmo maticial

## Requisitos
    -NodeJs 10.16.0 LTS

## Ejecución

Para ejecutar el programa escriba el siguiente comando

    node test3.js

Una ves escrito este comando se lanzara un CLI donde usted debera ingresar el numero de pruebas a realizar y las dimensiones de las matrices:  

**Ejemplo:**

    Input: 

    Type number of test to do: 2

    Type N for Matrix #1: 4
    Type N for Matrix #1: 7

    Type N for Matrix #2: 13
    Type N for Matrix #2: 25
```js
Output:

[ 4, 7 ]
[ 13, 25 ]
    
Calculating...
    
[ 'L', 'R' ]
```    
## Explicación

1. Se tiene una matriz inicial de tamaño N x M, cuando nos desplazamos inicialmente por la derecha hasta llegar al limite podemos, hacer una **sub-matriz de N-1 x M que representa los espacios disponibles**.  
2. Nuevamente nos desplazamos hasta el limite en direccion vertical, por lo cual ahora tenemos una **nueva matriz de N-1 x M-1** la cual llamaremos N'x M'.  
3. El proceso se repite multiples veces hasta que no pueda generarse una sub-matriz más, es decir cuando la multiplicacion de **N x M = 0**, lo cual se daría por una combinacion **0 x M'** o **N' x 0**.  
4. Como en cada iteracion se reduce en -1 N y M sucesivamente, llegamos a la conclucion que la condicion del paso tres se puede cumplir dependiendo de los valores de N y M :  
    
        Caso 1: N > M:
        
        El algoritmo termina en direccion vertical con "Up" o "Down" como posibles respuestas ya que M llegara primero a 0

        Caso 2  N = M:

        El algoritmo termina en direccion horizontal con "Right" o "Left" como posibles respuestas ya que N llegara primero a 0

        Caso 3  N < M:
        
        El algoritmo termina en direccion horizontal con "Right" o "Left" como posibles respuestas ya que N llegara primero a 0

5. Para determinar la orientacion exacta se evaluan los casos iniciales.  

    Caso 1 direccion vertical:

    Puesto que M llegará primero a 0 se debe determinar el numero de rotaciones que hara antes de llegar a el, como la primera rotacion posible apunta hacia abajo, podemos determinar que cuando **M es impar** siempre mirara hacia abajo mientras que **si es par** mirara hacia arriba

    Caso 2 direccion horizontal:

    Siguiendo la premisa anterior determinamos que si **N es impar** este mirara haca la derecha, **en caso contrario** mirara a la izquierda

6. Con todas las aseveraciones anteriores el problema se reduce a las siguientes operaciones:  

        N > M 
            Si M impar => Down
            Si M par => Up
        N = M o N < M
            Si N impar => Right
            Si N par => Left

