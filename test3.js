const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  })
var T,Mat=[],Mat2,t=0,rotation=[],positions2=[]; 

const  states = ['R','D','L','U'];

function toInteger(inp){
    //let a = Number(inp);
    if(!isNaN(inp)) return parseInt(inp);
    else return 0;
}

function makeQuestion(question,limit = -1){
    return new Promise((resolve,reject)=>{
        readline.question(question, (inp) => {
            let a = toInteger(inp);
            if(limit=== -1){
                if(a>0 && a<5001) resolve(a);
                else reject({error:"Please provide a number between 1-5000"});            
            }
            else{
                if(a>0) resolve(a);
                else reject({error:"Please provide a valid number "});            
            };
          });
    })
}

async function run(){
    while(T===undefined){
        try {
            T = await makeQuestion('Type number of test to do: ');        
            console.log('Number of test: '+T);
        }
        catch (error) {
            console.log(error.error);
        }
    }
    while(t<T){
        try {
            let N = await makeQuestion(`Type N for Matrix #${t+1}: `,0);  
            let M = await makeQuestion(`Type M for Matrix #${t+1}: `,0);
            Mat.push([N,M]);
            printMatix(Mat);
            t++;
        } 
        catch (error) {
            console.log(error.error);
        }
    }
    Mat2 = JSON.parse(JSON.stringify(Mat));

    /*
    console.log("Start method 1")
    let t0 = new Date()
    calculate(Mat);
    let t1 = new Date()
    console.log("Result method 1:("+(t1-t0)+"ms)");
    let positions = rotation.map((x)=>{return states[x]});
    console.log(positions);
    */
    
    //console.log("Start method 2")
    //let t2 = new Date()
    console.log("Calculating...")
    calculateFix(Mat2);
    //let t3 = new Date()
    //console.log("Result method 2:("+(t3-t2)+"ms)");
    console.log(positions2);
    
    readline.close();
   
}

function printMatix(mat){
    mat.forEach((element)=>{
        console.log(element);
    });
}
run();

function calculate(mat){
    mat.forEach((element)=>{
        
        let rot = 0;
        while(element[0] !== 0 && element[1] !==0){
            if(rot%2 == 0){
                element[0]=element[0]-1
            }
            else{
                element[1]=element[1]-1
            }
            if(element[0] !== 0 && element[1]!==0){
                rot++;
            }
            
        }
        rotation.push(rot%4);
    })
}

function calculateFix(mat){
    mat.forEach((element)=>{
        if(element[0]>element[1]){
            if(element[1]%2==0){
                positions2.push('U')
            }
            else{
                positions2.push('D')
            }
        }
        else{
            if(element[0]%2==0){
                positions2.push('L')
            }
            else{
                positions2.push('R')
            }
        }

    })
}


